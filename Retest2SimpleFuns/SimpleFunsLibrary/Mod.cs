﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFunsLibrary
{
    public class Mod : IntUnaryFun
    {
        int n = 1;
        public Mod(int n) {
            this.n = n;
        }
        public int Apply(int x)
        {
            return x % n;
        }

        public override string ToString()
        {
            return ("Деление с остатком");
        }
    }
}
