﻿using NUnit.Framework;
using System;
using SimpleFunsLibrary;

namespace UnitTest
{
	[TestFixture ()]
	public class Test
	{
		[Test ()]
		public void TestIncrement()
		{
			//Arange
			int source = 5;
			Increment testedClass = new Increment();
			//act
			int res = testedClass.Apply(source);
			//Assert
			Assert.AreEqual(6, res);
		}

		[Test ()]
		public void TestAdd()
		{
			//Arange
			int source = 5;
			int adding = 3;
			Add testedClass = new Add(source);
			//act
			int res = testedClass.Apply(adding);
			//Assert
			Assert.AreEqual(8, res);
		}

		[Test ()]
		public void TestDecrement()
		{
			//Arange
			int source = 5;
			Decrement testedClass = new Decrement();
			//act
			int res = testedClass.Apply(source);
			//Assert
			Assert.AreEqual(4, res);
		}

		[Test ()]
		public void TestMod()
		{
			//Arange
			int source = 7;
			int div = 15;
			Mod testedClass = new Mod(source);
			//act
			int res = testedClass.Apply(div);
			//Assert
			Assert.AreEqual(1, res);
		}


		[Test ()]
		public void TestPow2()
		{
			//Arange
			int source = 7;
			Pow2 testedClass = new Pow2();
			//act
			int res = testedClass.Apply(source);
			//Assert
			Assert.AreEqual(49, res);
		}

	}
}



