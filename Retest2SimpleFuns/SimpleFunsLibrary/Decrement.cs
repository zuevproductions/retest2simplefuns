﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleFunsLibrary;

namespace SimpleFunsLibrary
{
    public class Decrement:IntUnaryFun
    {
        public int Apply(int x)
        {
            return x - 1;
        }

        public override string ToString()
        {
            return ("Декремент");
        }
    }
}
