﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using SimpleFunsLibrary;


namespace Retest2SimpleFuns
{
    class Program
    {
        static void Main(string[] args)
        {

            List<IntUnaryFun> v = new List<IntUnaryFun>();

            v.Add(new Increment());
            v.Add(new Pow2());
            v.Add(new Mod(50));
            v.Add(new Add(50));
            v.Add(new Decrement());
            //деление с остатком бинарная функция
            int number = 148;
            foreach (IntUnaryFun f in v)
            {
                Console.WriteLine(number);
                Console.WriteLine(f.ToString());
                number = f.Apply(number);
                Console.WriteLine(number);
                Console.WriteLine();
            }
            Console.ReadKey();
        }
    }
}
