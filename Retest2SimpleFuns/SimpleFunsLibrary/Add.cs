﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleFunsLibrary
{
    public class Add : IntUnaryFun
    {
        int n = 1;
        public Add(int n)
        {
            this.n = n;
        }
        public int Apply(int x)
        {
            return x + n;
        }

        public override string ToString()
        {
            return ("Прибавить");
        }
    }
}

