﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleFunsLibrary;

namespace SimpleFunsLibrary
{
    public class Pow2:IntUnaryFun
    {
        public  int Apply(int x)
        {
            return x * x;
        }

        public override string ToString()
        {
            return ("Возведение в квадрат");
        }
    }
}
